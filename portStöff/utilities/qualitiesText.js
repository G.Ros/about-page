const qualitiesCardsText = [
  {
    heading: "Creative",
    info:
      "I’ve been involved in creative activities since I was a kid. From making short films, being in bands, to drawing and making street art.",
    id: 1,
  },
  {
    heading: "Design",
    info: `I’ve enjoyed drawing since I was a kid, so I have a good sense of aesthetics. 
      I consider myself a good designer though I have plenty of room to grow.`,
    id: 2,
  },
  {
    heading: "Team player",
    info: `I enjoy being a part of a team and contributing to a collective effort.`,
    id: 3,
  },
  {
    heading: "Quality",
    info: `When I do things I like to do them well and I don’t mind getting my hands dirty to get them right.`,
    id: 4,
  },
];

export default { qualitiesCardsText };
