const programmingCardsText = [
  {
    heading: "HTML",
    info: `Very good understanding of the behavior of HTML elements and
              best practices.`,
    rating: "95%",
    id: 1,
  },
  {
    heading: "CSS",
    info: `Good general knowledge and a good understanding of; CSS box model, CSS flexbox module, CSS grid layout module, and @media rules implementation`,
    rating: "90%",
    id: 2,
  },
  {
    heading: "Sass",
    info: `Good knowledge and experience of the preprocessor scripting language Sass.`,
    rating: "80%",
    id: 3,
  },
  {
    heading: "Javascript",
    info: `Good fundamental understanding of Javascript. Good knowledge of Javascript frameworks, like React.js and Gatsby.js.`,
    rating: "70%",
    id: 4,
  },
  {
    heading: "React.js",
    info: `Good understanding and experience working with class components, lifecycle methods, functional components and React hooks. Decent understanding and experience working with Context API.`,
    rating: "70%",
    id: 5,
  },
  {
    heading: "Gatsby.js",
    info: `Good fundamental understanding of Gatsby.js. Experience working with Gatsby Image API and implementing various plugins.`,
    rating: "65%",
    id: 6,
  },
  {
    heading: "React native",
    info: `Pretty good knowledge and experience working with React native.`,
    rating: "45%",
    id: 7,
  },
  {
    heading: "Github",
    info: `Good knowledge of the basics of using Github.`,
    rating: "40%",
    id: 8,
  },
  {
    heading: "Bootstrap",
    info: `Good knowledge of the CSS framework Bootstrap.`,
    rating: "75%",
    id: 9,
  },
  {
    heading: "Database",
    info: `Decent knowledge of the MySql database. Good knowledge of Google’s Firebase database.`,
    rating: "50%",
    id: 10,
  },
];

export default { programmingCardsText };

// const qualitiesCardsText = [
//   {
//     heading: "Creative",
//     info:
//       "I’ve been involved in creative activities since I was a kid. From making short films, being in bands, to drawing and making street art.",
//     id: 1,
//   },
//   {
//     heading: "Design",
//     info: `I’ve enjoyed drawing since I was a kid, so I have a good sense of aesthetics.
//     I consider myself a good designer though I have plenty of room to grow.`,
//     id: 2,
//   },
//   {
//     heading: "Team player",
//     info: `I enjoy being a part of a team and contributing to a collective effort.`,
//     id: 3,
//   },
//   {
//     heading: "Quality",
//     info: `When I do things I like to do them well and I don’t mind getting my hands dirty to get them right.`,
//     id: 4,
//   },
// ];

// export default { programmingCardsText };
