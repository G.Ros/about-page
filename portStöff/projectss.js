import React from "react";
import Layout from "../components/layouts/index";
import FernwehProject from "../components/cards/projects/fernweh";
import RosaProject from "../components/cards/projects/rosa-olof";
import "@brainhubeu/react-carousel/lib/style.css";

const Projects = () => {
  return (
    <Layout>
      <div id="projects-page">
        <h2 className="hero-heading-smaller">Projects</h2>
        <div className="projects-container">
          <FernwehProject />
          <RosaProject />
        </div>
      </div>
    </Layout>
  );
};

export default Projects;
