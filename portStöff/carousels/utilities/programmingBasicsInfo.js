import React from "react";

const Test = () => {
  const programmingBasicsInfo = [
    {
      heading: "GraphQl",
      info: `Intermediate knowledge of the data querying language, GraphQL.`,
      rating: "40%",
      id: 1,
    },
    {
      heading: "HTML & CSS",
      info: `Solid understanding of the behaviour of HTML elements and best practices. Good general knowledge understanding of:
        CSS box model, CSS flexbox module, CSS grid layout module and @media rules implementation.`,
      rating: "90%",
      id: 2,
    },
    {
      heading: "Javascript",
      info: `Good knowledge and experience using the scripting language Javascript.`,
      rating: "70%",
      id: 3,
    },
    {
      heading: "Sass",
      info: `Good knowledge and experience of the preprocessor scripting language Sass.`,
      rating: "80%",
      id: 4,
    },

    {
      heading: "Typescript",
      info: `Intermediate knowledge of the strongly typed programming language Typescript.`,
      rating: "40%",
      id: 5,
    },
  ];

  return (
    <>
      {programmingBasicsInfo.map((data) => (
        <div className="programming-card" key={data.id}>
          <h3 className="scndry-heading">{data.heading}</h3>
          <div className="info-graphic">
            <span className="label">{data.rating}</span>
          </div>
          <p className="main-text">{data.info}</p>
        </div>
      ))}
    </>
  );
};

export default Test;
