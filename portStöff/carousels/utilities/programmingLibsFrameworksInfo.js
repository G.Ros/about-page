const programmingLibsFrameworksInfo = [
  {
    heading: "Angular",
    info: `Good understanding of components, templates, directives, services, modules and lifecycle hooks.`,
    rating: "60%",
    id: 1,
  },
  {
    heading: "Bootstrap",
    info: `Good knowledge of the CSS framework Bootstrap.`,
    rating: "70%",
    id: 2,
  },
  {
    heading: "Gatsby.js",
    info: `Good fundamental understanding of Gatsby.js. Experience using Gatsby-image-plugin. Experience using Gatsby with Wordpress as a headless cms coupled with Netlify for continous deployment.`,
    rating: "75%",
    id: 3,
  },
  {
    heading: "React.js",
    info: `Good understanding and experience working with class components, lifecycle methods, functional components and React hooks. Decent understanding and experience working with Context API.`,
    rating: "75%",
    id: 4,
  },
  {
    heading: "React native",
    info: `Intermediate knowledge and experience working with React native.`,
    rating: "50%",
    id: 5,
  },
  {
    heading: "RxJs",
    info: `Basic understanding of the reactive programming library RxJs.`,
    rating: "40%",
    id: 6,
  },
];

export default programmingLibsFrameworksInfo;
