const programmingMiscInfo = [
  {
    heading: "Adobe Illustrator",
    info: `Basic knowledge and experience using Adobe Illustrator.`,
    rating: "40%",
    id: 1,
  },
  {
    heading: "Figma",
    info: `
    Good knowledge and experience using the design software Figma.`,
    rating: "80%",
    id: 2,
  },
  {
    heading: "Git",
    info: `Decent knowledge and experience using version control systems like Github and Gitlab.`,
    rating: "60%",
    id: 3,
  },
  {
    heading: "Database",
    info: `
    Good general knowledge and experience working with MySql and Firebase db’s.`,
    rating: "65%",
    id: 4,
  },
  {
    heading: "Wordpress",
    info: `Experience using Worpress as a headless cms with Gatsby.js, coupled with Netlify for continous deployment.`,
    rating: "50%",
    id: 5,
  },
];

export default programmingMiscInfo;
