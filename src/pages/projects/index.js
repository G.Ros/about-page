import React from 'react';
import Seo from '../../components/Seo';
import Layout from '../../components/layouts/index';
import FernwehProject from '../../components/cards/projects/fernweh';
import RosaProject from '../../components/cards/projects/rosa-olof';
import WotschProject from '../../components/cards/projects/wotsch';

const Projects = () => {
	return (
		<Layout>
			<Seo title='Projects' />
			<div id='projects-page'>
				<h2 className='hero-heading-smaller'>Projects</h2>
				<div className='projects-container'>
					<WotschProject url={'wotsch'} />
					<RosaProject url={'rosa'} />
					<FernwehProject url={'fernweh'} />
				</div>
			</div>
		</Layout>
	);
};

export default Projects;
